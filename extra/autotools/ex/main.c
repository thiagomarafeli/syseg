/* main.c - A simple hello-world program.

   Copyright (c) 2014, Francisco Jose Monaco  <monaco@usp.br>

   This file is part of POSIXeg

   POSIXeg is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "foobar.h"
#include "config.h"

#define MSG "xpto"

#if HAVE_DECL_STRDUP == 0
char *strdup (const char*);
#endif

int main (int argc, char **argv)
{
  char *s;
  
  printf (PACKAGE_STRING "\n");

  foo();
  bar();

  s = strdup(MSG);

  printf ("String: %s\n", s);

  
  return EXIT_SUCCESS;
}
