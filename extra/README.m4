include(docm4.m4)

 EXTRA RESOURCES
 ==============================

DOCM4_DIR_NOTICE

 Overview
 ------------------------------

 This directory contains extra examples which complement the contents addressed
 in the major SYSeg chapters. They cover additional source code illustration,
 introduction to some advanced topics, non-standard features, hacker lore,
 tips on tools and techniques.

 Contents
 ------------------------------

 Subdirectories

 - make		Make script to build several binaries and libs (GNU make)
 - autotools	A brief tutorial on the GNU Build System (autoconf, automake)
