include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Make script])

init: all 

UPDATE_MAKEFILE

##
## Relevant rules
##

CPP_FLAGS = $(CPPFLAGS)
C_FLAGS   = -m32 -g $(CFLAGS)
LD_FLAGS  = -m32 -L. $(LDFLAGS)

all: cypherbin crypt

cypherbin : cypherbin.o
	gcc $(LD_FLAGS) $< -o $@

%.o : %.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) $< -o $@

libcypher.so : cypher.o
	gcc --shared $(LD_FLAGS) $< -o $@

crypt : crypt.o | libcypher.so
	gcc $(LD_FLAGS) $< -Wl,-rpath='$$ORIGIN' -o $@ -lcypher

.PHONY: clean

clean :
	rm -f cypherbin crypt *.o *.so
	rm -f *~

dnl
dnl Uncomment to include bintools
dnl
dnl
dnl DOCM4_MAKE_BINTOOLS

