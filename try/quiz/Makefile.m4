include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Make script])

init: all 

UPDATE_MAKEFILE

##
## Relevant rules
##

CPP_FLAGS = $(CPPFLAGS) 
C_FLAGS   = -Og $(CFLAGS)
LD_FLAGS  = $(LDFLAGS)

bin-rel = eg-01
bin-pic = eg-02

bin = $(bin-rel) $(bin-pic)

all: $(bin) 

$(bin-rel) : % : %-rel.o
	gcc -m32 -no-pie $(LD_FLAGS) $^ -o $@ 

%-rel.o : %.c
	gcc -m32 -c -fno-pic -fno-pie $(CPP_FLAGS) $(C_FLAGS) $< -o $@

$(bin-pic) : % : %-pic.o
	gcc -m32 -pie $(LD_FLAGS) $^ -o $@ 

%-pic.o : %.c
	gcc -m32 -c -fpic -fpie $(CPP_FLAGS) $(C_FLAGS) $< -o $@



.PHONY: clean

clean :
	rm -f $(bin) *.o

dnl
dnl Uncomment to include bintools
dnl
dnl
DOCM4_MAKE_BINTOOLS

