include(docm4.m4)

 Title
 ==============================

 DOCM4_DIR_NOTICE

 Overview
 ------------------------------

 This directory contains a collection of short questions exploring
 System Softare concepts. It may be used as a quiz to evaluate one's
 background and skills in the field.

dnl
dnl Uncomment to include docm4 instructions
dnl
 DOCM4_INSTRUCTIONS

 Contents
 ------------------------------

 * eg-01.c

 Challenges
 ------------------------------

 1) Consider the program eg-01.c

    Build and execute it:
    
	make eg-01


    a) Where exactly

dnl
dnl Uncomment to include bintools instructions
dnl 
dnl DOCM4_BINTOOLS_DOC

# Questions
# How value is communicated between
