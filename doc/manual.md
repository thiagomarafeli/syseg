
SYSeg - System Software by example.

Copyright 2021 (c) by Monaco F. J. <monaco@usp.br>

SYSeg is distributed under the terms of the GNU General Public License v3.

 SYSeg Manual
==============================

TL;DR

    	
- As part of my undergraduate research program, I did some work on 
  scientific instrumentation at the Institute of Physics, where 
  there was this huge and intimidating high-tech microscopy equipment 
  that mainly resembled some alien artifact from a scifi movie. 
  A stainless-steel tag attached to it read:

  "_If all attemps to assemble this device fail, read the instruction manual._"


INTRODUCTION
------------------------------

SYSeg (System Software by example) is a collection of code examples and programming
exercises intended for illustration of general concepts and techniques concerning
system software design and implementation. It may be useful as a companion 
resource for students and instructors exploring this field.

SYSeg is free software :-D

BASIC INFORMATION
------------------------------

Each subdirectory in the source tree has a file `README` wich contains important
instructions on how use the directory's contents. 

Please, as the the name sugests, do read the information --- that will guaranteedly save you time.

If you find a file named `README.m4`, this is not the file you're looking for; 
it's a source file that should be used to create the corresponding `README`.
If you find the source `m4` file but no respective `README` file in the same
directory, this is because you haven't performed the project's configuration procedure described bellow. In this case, please go through the setup instructions and proceed as explained.


SETUP INSTRUCTIONS
------------------------------
 
Some examples in SYSeg need auxiliary artifacts which must be built beforehand.

If you have obtained the project source from the __version control repository__,

i.e. you've cloned the project from its official Git repository, execute the script 

 ```
 $ ./bootsrap.sh
 ```

to create the build configuration script `configure`. To that end, you'll 
need to have GNU Build System (Autotools) installed. In Debian/Ubuntu based 
platforms,  you may install the required software with

```
$ sudo apt install automake autoconf
```

On the other hand, if you have obtained the software form a __distribution 
repository__, usually as a tarball, you should have the  script `configure`
already built, and therefore you may safely skip the previous step.

Either way, locate the file in the root of source directory tree and execute it

```
 $ ./configure
```

This script shall perform a series of tests to collect data about the build 
platform. If it complains about missing pieces of software, install them 
as needed and rerun `configure`. The file `syseg.log` contains a summary
of the last execution of the configuration script.

Finally, build the software with

```
 $ make
 ```

**Note** that, as mentioned, this procedure is intended to build the auxiliary tools needed by SYSeg. The examples and programming exercises themselves will not be built as result of the main configuration script being executed. The process of building the example code is part of the skills those are meant 
to exercise.

To build each example or programming exercise, one need to acess the directories containing the respective source code and follow the instructions indicated in the included README file.

For more detailed instructions on building SYSeg, please, refer to file `INSTALL` in the root of the source tree.

 CONFIGURATION OPTIONS
 ------------------------------

 These are some options you may want to configure.

 If you make any changes, remember to rebuild SYSeg as described above.

 __Configure diff tool__
 
  SYSeg uses a graphical diff tool to help visual comparison among code artifacts.
   You may choose which tool you prefer by editing

      DIFF_TOOL = meld

  in syseg/tools/bintools.m4

  SYSeg CONTENTS
  -------------------------------


 - Directory `eg` contains source code examples.
 - Directory `try` contains programming exercises.
 - Directory `extra` contains non-standard features, advanced topics, and hacker ore.
 - Directory `tools` contains auxiliary tools used by examples and exercises.
 - Directory `doc` contains SYSeg documentation.

  
 PROPOSED EXERCISES
 ------------------------------
 
 In addition to code examples, SYSeg contains also some proposed programming
 exercises which can be found in directory `syseg/try`

 Students interested in system software are encouraged to explore them. If
 this is your case, please read on for general directions.

 Instructors are welcome as well to use SYSeg's exercises to teach their
 students. SYSeg provides some handy tools for this purpose, including
 scripts to prepare bootstrap code and directions to deliver the activities
 through a VCS repository.

 If you reuse SYSeg code either in your classes or implementation project,
 you are kindly invited to drop the author a message. It's always nice to
 know who else is exploring the material.

 __Important__

 If the specification of an exercise provides some bootsrap code, please,
 do not simply copy the files to your directory. Instead, proceed as
 indicated in the README file found in the respective directory.

 When you are finished with the exercise, deliver your working using the
 method described in the aforementioned README file. The directions
 will possibly instruct you to commit your changes into a VCS (e.g. Git)
 repository.

 CONTRIBUTING
 -----------------------------------

Bug reports and suggestions are always welcome!

Please contact the author.

 CONVENTIONS USED IN THIS PROJECT
 ----------------------------------

 Unless otherwise explicitly annotated, the project documentation uses
 the following conventions.

 - Values in decimal base are denoted in their usual form as 1, 15, 42.
   Values in hexadecimal base are denoted with the prefix 0x, as in
   0x1, 0xF, 0x2H, or else by the suffix h, as in 1h, 2Ah.

 - When referring to memory capacity, the symbols b is used to indicate
   a bit of information, while the symbols B and Byte are used to indicate
   an 8-bit byte. 

 - The symbols K, M, G etc. are used to denote the power-of-two multiples
   of a unit.

    E.g. 4 KBytes = 4 * 1024 Bytes.
    	 1 Gb = 1024 * 1024 b = 1048576 b = 400h bytes.

   Note:

   In the context of digital transmission, the electrical engineering
   community tends to prefer using power-of-ten multiples, like in
   10 Kbps (meaning 10 * 1000 bits per second). On the other hand, computer
   engineers, when referring to memory size, tend to prefer using
   power-of-two multiples, as in 1 GB (meaning 1024 * 1024 Bytes).
   This may be a source of confusion, for instance, when measuring the
   capacity of storage devices. We'll use power-of-two. To avoid that,
   some references use the prefix Ki and Mi to indicate power-of-two
   multiples, and distinguish them from the corresponding power-of-ten
   multiples of the SI (International System of Units). Others use
   K (capital) to mean 2^10 and k (small caps) to mean 10^3 --- as
   the SI symbol for kilo is k with small caps. Our call is to use
   power-of-two multiples, and explicitly highlight if power-of-tem
   multiple is eventually implied.

 TROUBLESHOOTING
 ------------------------------   
   
 If you ever find any trouble using SYSeg, consider the following tips.

 - __I can see a `README.m4` or a `Makefile.m4` file in a given directory, but can't find the corresponding `README` or `Makefile` file.__

   This probably happens because you've missed the installation instructions.
   
   To fix the issue:
   
   ```
   cd <path>/syseg
   ./bootstrap.sh
   ./configure
   make
   ``` 
 - __I edited a Makefile.m4 and need to update the corresponding Makefile__
 
   Within the directory containg the edited `Makefile.am`:
   
   ```
   make -C ..
   ```
   
   