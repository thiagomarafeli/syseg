include(docm4.m4)

 Title
 ==============================

DOCM4_DIR_NOTICE

 Overview
 ------------------------------

dnl
dnl Uncomment to include docm4 instructions
dnl
DOCM4_INSTRUCTIONS

 Contents
 ------------------------------

 * getpid.c	Usage of getpid() and getppid().

   		Build, execute and inspect the running process with

		   ps uf

 		Observe that getpid's is a child of the shell.

		The child's return status is informed to the parent:

		  echo $?

 * fork.c       Duplicate the caller process.

 * fork-01.c    Execute different code in parent and child.

 * fork-02.c	What happens if either parent or child terminates first.

 * exec.c	Exec a binary program.

 * wait.c	Wait for child to terminate.

 * shell.c	A very simple shell skeleton.

 * shell-01.c	Like shell.c, but accepting program arguments.

 * pipe.c 	IPC with pipes.

 * 

dnl
dnl Uncomment to include bintools instructions
dnl 
dnl DOCM4_BINTOOLS_DOC

