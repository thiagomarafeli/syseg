#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "debug.h"

int main()
{

  printf ("Controlling terminal: %s\n", ctermid(NULL));

  printf ("TTY name:             %s\n", ttyname(0));
  
  return EXIT_SUCCESS;
}

