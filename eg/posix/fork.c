#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
  
  fork();
  
  printf ("Hi, my pid is %d and my parent's is %d\n",
	  getpid(), getppid());

  return EXIT_SUCCESS;
}
