#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  pid_t pid, ppid;

  pid = getpid();		/* Get caller's PID.               */

  ppid = getppid();		/* Get PID of the caller's parent. */

  printf ("Pid: %d, parent pid: %d\n", pid, ppid);

  sleep(10);			/* Inspect running process with ps.*/
  
  return 7;
}
