
dnl  Makefile.m4 - Makefile template.
dnl    
dnl  Copyright (c) 2021 - Monaco F. J. <monaco@usp.br>
dnl
dnl  This file is part of SYSeg. 
dnl
dnl  SYSeg is free software: you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation, either version 3 of the License, or
dnl  (at your option) any later version.
dnl
dnl  SYSeg is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with .  If not, see <http://www.gnu.org/licenses/>.
dnl
dnl  ------------------------------------------------------------------
dnl
dnl  Note: this is a source file used to produce either a documentation item,
dnl        script or another source file by m4 macro processor. If you've come
dnl	   across a file named, say foo.m4, while looking for one of the
dnl	   aforementioned items, changes are you've been looking for file foo,
dnl	   instead. If you can't find foo, perhaps it is because you've missed
dnl 	   the build steps described in the file README, found in the top 
dnl	   source directory of this project.
dnl        
dnl
include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

## 
## Stage 1: boot loader
## Stage 2: kernel
##

bin = boot.img

# Stage 1 uses some basic functions implemented in utils.c

bootloader_obj = bootloader.o utils.o

# Stage 2 also uses utils.c, but includes a lot more implemented in tyos.c

kernel_obj = kernel.o 

# Size of kernel in 512-byte sectors

all : $(bin) 

UPDATE_MAKEFILE

DOCM4_RELEVANT_RULES

## 
## First and second stage, plus auxiliary items.
## 

boot_obj = bootloader.o kernel.o utils.o init32.o

boot.bin : $(boot_obj) boot.ld rt0.o 
	ld -melf_i386 --orphan-handling=discard  -T boot.ld $(boot_obj) -o $@

bootloader.o utils.o  : %.o: %.c
	gcc -m16 -O0 -I. -Wall -fno-pic -fcf-protection=none  --freestanding -c $<  -o $@

kernel.o : %.o : %.c
	gcc -m32 -O0 -I. -Wall -fno-pic -fcf-protection=none  --freestanding -c $<  -o $@

init32.o : init32.S
	as -32 $< -o $@

k.o : k.S
	as -32 $< -o $@

bootloader.o kernel.o utils.o  : utils.h
kernel.o : 

rt0.o : rt0.S
	gcc -m16 -O0 -Wall -c $< -o $@

# gdt.o : gdt.S
# 	as -32 $< -o $@

# init32.o : init32.c
# 	gcc -m32 -c $(C_FLAGS) $< -o $@


# Create a 1.44M floppy disk image (which is actually 1440 KB)

floppy.img: boot.bin
	rm -f $@
	dd bs=512 count=2880 if=/dev/zero of=$@ # Dummy 1440 kB floppy image.
	mkfs.fat -R 2 floppy.img

# Write boot.bin at begining of the floppy image.

boot.img : boot.bin floppy.img
	cp floppy.img $@
	dd conv=notrunc ibs=1 obs=1 skip=62 seek=62 if=boot.bin of=$@




#
# Housekeeping
#

clean:
	rm -f *.bin *.elf *.o *.s *.iso *.img *.i
	make clean-extra

clean-extra:
	rm -f *~ \#*


#
# Programming exercise
#

EXPORT_FILES  = utils.c  utils.h rt0.S  bootloader.c  kernel.c  boot.ld
EXPORT_FILES += Makefile
EXPORT_FILES += README ../../tools/COPYING 


DOCM4_EXPORT(tyos,0.1.0)


# Include Make Bintools

DOCM4_MAKE_BINTOOLS

