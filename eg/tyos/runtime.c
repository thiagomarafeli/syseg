/* runtime.c - C source file.
 
   Copyright (c) 2020-2022 - Monaco F. J. <monaco@usp.br>

   This file is part of SYSeg. 

   SYSeg is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   SYSeg repository is accessible at https://gitlab.com/monaco/syseg

*/

#include "syscall.h"

/* Issue syscall number with arguments arg1, arg2, arg3. 
   Return the value in %ax. */

int syscall (int number, int arg1, int arg2, int arg3)
{
  int register ax __asm__("ax"); /* Declare variables are registers. */
  int register bx __asm__("bx");
  int register cx __asm__("cx");
  int register dx __asm__("dx");
  int ax2, bx2, cx2, dx2;
  int status;

  /* Save current registers. */
  ax2 = ax;
  bx2 = bx;
  cx2 = cx;
  dx2 = dx;

  /* Load registers. */
  ax = number;
  bx = arg1;
  cx = arg2;
  dx = arg3;
  
  __asm__("int $0x21");

  status = ax;
  
  /* Restore registers. */
  ax = ax2;
  bx = bx2;
  cx = cx2;
  dx = dx2;

  return status;
}


int puts (const char *buffer)
{
  int status;
  status = syscall (SYS_WRITE, (int) buffer, 0, 0);
  return status;
  
}

