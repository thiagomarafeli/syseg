
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/wait.h>

int main()
{
  int pid;			/* Process id.         */
  int status;			/* Exit status.        */
  int count;			/* Auxiliary variable. */

  /* Function fork creates a duplicate of the current process.
     Upon the function return, there will be two process: the caller 
     --- aka the parent --- process, and the newly created --- aka
     the child --- process.

     To the parent process, fork() returns the PID of the child.
     To the child, fork() returns zero.  */
    
  pid = fork();

  if (pid > 0)		/* Parant process. */
    {
      count = 5;
      while (count--)
	{
	  printf ("%d Parent: my pid is %d; my child's pid is %d\n", count, getpid(), pid);
	  sleep (1);
	}
      wait (&status);		/* Block until a child returns. */
    }
  else			/* Child process. */
    {
      count = 10;
      while (count--)
	{
	  printf ("%d Child:  my pid is %d; my parent's pid is %d\n", count, getpid(), getppid());
	  sleep (1);
	}
      printf ("Child done.");

    }

  return EXIT_SUCCESS;
}

/* Notes.

   In this example, the parent process finishes first and becomes blocked at wait()
   until the child finishes. The child exit status (along with other information
   is collected by wait and enconded into its argument 'status'. */
