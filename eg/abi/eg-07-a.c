
int foo()
{
  int register var __asm__("ecx");
  var += 1;
  return var;
}

int main ()
{
  foo();
  return 0;
}
