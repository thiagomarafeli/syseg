/* core.c -  Core functions.
 
   Copyright (c) 2021, Monaco F. J. <monaco@usp.br>

   This file is part of SYSeg.

   SYSeg is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <utils.h>


/* Prints string in buffer on the screen.  */

void __attribute__((fastcall, naked))  echo (const char* buffer)
{
__asm__
(
 "        pusha                      ;" /* Save all registers.               */
 "        mov   %cx, %bx             ;"
 "        mov   $0x0e, %ah           ;" /* Video BIOS service: teletype mode */
 "        mov   $0x0, %si            ;" 
 "loop:                              ;"                                    
 "        mov   (%bx, %si), %al      ;" /* Read character at bx+si position. */
 "        cmp   $0x0, %al            ;" /* Repeat until end of string (0x0). */
 "        je    end                  ;"                                    
 "        int   $0x10                ;" /* Call video BIOS service.          */
 "        add   $0x1, %si            ;" /* Advace to the next character.     */
 "        jmp   loop                 ;" /* Repeat the procedure.              */
 "end:                               ;"
 "        popa                       ;" /* Restore all registers.             */
 "        ret                        ;" /* Return from this function.         */

 );
}

/* For debugging. */

void __attribute__((naked, fastcall)) fatal(const char *msg)
{
  echo (msg);
  echo (NL);
    
  __asm__ 
    (
     "fatal_halt2:       ;"		/* Halt the CPU. */
     "   hlt;           ;"		/* Make sure it */
     "   jmp fatal_halt2 "		/* remains halted. */
     );
}


/* Clear the screen and set video colors. */

void __attribute__((naked, fastcall)) clear (void)
{

  __asm__ 
    (
     " pusha                            ;" /* Push all registers. */
     " mov $0x0600, %ax                 ;" /* Video BIOS service: Scroll up. */
     " mov $0x07, %bh                   ;" /* Attribute (back/foreground).   */
     " mov $0x0, %cx                    ;" /* Upper-left corner.             */
     " mov $0x184f, %dx                 ;" /* Upper-right corner.            */
     " int $0x10                        ;" /* Call video BIOS service.       */
     " popa                             ;" /* Pop all registers. */
     " ret                               " /* Return from function. */
     );

}

void system_halt()
{

  __asm__
    (
     "sys_halt:            ;"
     "        hlt          ;"
     "        jmp sys_halt ;"
     );
}
  


/* Notes.



 */
